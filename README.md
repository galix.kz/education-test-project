
### Task: Contribute to Edu-Platform Development

[link to design](https://www.figma.com/file/kCr7iGaMQbtRTnGuQlA1Y4/Bolashak?type=design&node-id=0-1&mode=design&t=KbpToypov5KdtaiP-0)

#### Objective:
Participate in the development of an online education platform that hosts video lessons and tests. Your work will focus on implementing and improving specific features within the platform, following the provided project structure.

#### Requirements:

- **Environment Setup**: Clone the project repository, set up your development environment, and ensure you can run the project locally.
- **Feature Development**: Select a feature from the project's issue tracker or assignment list. Features may include user interfaces, backend services, or database management.
- **Code Quality**: Write clean, efficient, and well-documented code. Follow the coding standards and guidelines provided in the project's README.
- **Testing**: Write unit, integration, and/or end-to-end tests for the feature you are implementing to ensure its functionality and reliability.
- **Version Control**: Use Git for version control. Create a feature branch for your work and commit changes with clear, concise messages.
- **Code Review**: Open a pull request for your branch when you're ready to merge your feature into the main codebase. Address any feedback received during the code review process.

#### Tasks:

1. **User Interface (UI)**: Create and style React components for the user interface. Ensure responsiveness and accessibility standards are met.
2. **State Management (MobX)**: Manage application state for your feature using MobX stores. Demonstrate proper use of observables and actions.
3. **Networking (Axios & SWR)**: Implement API calls for data retrieval and submission using Axios, and use SWR for data fetching, caching, and revalidation.
4. **Database Interaction**: Design and modify database schemas, write migration scripts, and implement seed data where necessary.
5. **Documentation**: Update the README.md with your feature documentation, setup instructions, and any other relevant information.
6. **Testing**: Write comprehensive tests for your code. Ensure high test coverage and document any specific testing instructions.

#### Evaluation Criteria:

- **Functionality**: Does the feature work as expected?
- **Code Quality**: Is the code clean, organized, and following the project's standards?
- **Testing**: Are the tests comprehensive and passing(Optional)?
- **Documentation**: Is the feature and its associated components properly documented(Optional)?
- **Peer Review**: How well did you address feedback from code reviews?

#### Submission:

Submit your pull request URL and report through the educational platform's submission portal by the deadline. Late submissions will be subject to penalty as per the course policy.

## Project structure
Creating a full project structure for an educational platform similar to Udemy requires comprehensive planning to cover various aspects such as video lessons, testing, user management, and content organization. Below is a proposed project structure that outlines the main directories and files for a modern web application using React, Next.js, MobX for state management, Axios for HTTP requests, and SWR for data fetching:

```
/edu-platform
|-- /public                     # Static files like images, fonts, and `robots.txt`
|-- /src                        # Source files for the application
|   |-- /components             # Reusable components
|   |   |-- /layout             # Components for layout like headers, footers
|   |   |-- /ui                 # UI-specific components like buttons, modals
|   |   `-- /forms              # Form components
|   |
|   |-- /pages                  # Next.js pages and entry points for routing
|   |   |-- /auth               # Authentication related pages (login, register)
|   |   |-- /courses            # Course listing and individual course pages
|   |   |-- /lessons            # Individual lesson pages
|   |   `-- /tests              # Test and quiz related pages
|   |   `-- _app.js             # Next.js custom App component
|   |   `-- _document.js        # Next.js custom Document component
|   |   `-- index.js            # Home page
|   |
|   |-- /models                 # Data models for courses, users, tests, etc.
|   |-- /hooks                  # Custom React hooks
|   |-- /utils                  # Utility functions and helpers
|   |-- /services               # Service functions to interact with backend API
|   |   |-- apiService.js       # Axios instance initialization
|   |   `-- courseService.js    # Course-related API calls
|   |
|   |-- /store                  # MobX stores for state management
|   |   |-- /CourseStore.js     # State management for courses
|   |   `-- /UserStore.js       # State management for user authentication
|   |
|   |-- /styles                 # Styles directory for CSS/SCSS
|   `-- /lib                    # Library code that is not components (e.g., SWR fetchers)
|
|-- /test                       # Test files (using Jest or similar)
|   |-- /unit                   # Unit tests
|   |-- /integration            # Integration tests
|   `-- /e2e                    # End-to-end tests (using Cypress or similar)
|
|-- /scripts                    # Build and deployment scripts
|-- /config                     # Project configuration files
|-- /migrations                 # Database migration scripts
|-- /seeds                      # Seed data for database
|
|-- .env.local                   # Local environment variables
|-- .env.production              # Production environment variables
|-- .babelrc                     # Babel configuration
|-- .eslintrc.js                 # ESLint configuration
|-- .gitignore                   # Specifies intentionally untracked files to ignore
|-- .prettierrc                  # Prettier configuration
|-- jest.config.js               # Jest configuration
|-- next.config.js               # Next.js configuration
|-- package.json                 # Project metadata and dependencies
|-- README.md                    # README for the project
`-- yarn.lock                    # Yarn lock file (or `package-lock.json` for npm)
```

This structure considers best practices for a scalable and maintainable codebase:

- **Components**: Organized to promote reusability.
- **Pages**: Using Next.js' file-based routing system.
- **Models**: Define the shape of data for consistency.
- **Hooks**: For encapsulating component logic.
- **Utils**: Utility functions that can be used throughout the app.
- **Services**: Abstract the API calls from the components.
- **Store**: Contains the state management logic using MobX.
- **Styles**: Local and global styles.
- **Lib**: Non-component libraries or helper functions, such as data fetching using SWR.
- **Tests**: Separated by type, making it clear where to find/add tests.
- **Config**: Centralized application configuration.
- **Migrations/Seeds**: For database versioning and initial data setup.
- **Environment files**: To manage different environments like local development and production.

This proposed structure will help you start building an educational platform with a clear directory layout and separation of concerns.
